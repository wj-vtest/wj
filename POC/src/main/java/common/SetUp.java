package common;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import io.appium.java_client.MobileDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import resources.Constants;

/**
 * @author Ranjit
 */
public class SetUp {
	public boolean pStatus=true;
	public String atest="";
	protected static MobileDriver driver;
	protected ExtentReports extentReports;
	protected ExtentTest logger;
	public String basefold,folder;
	public static int count=1;
	@BeforeSuite
	public void reportGeneration() {
		
	    SimpleDateFormat df = new SimpleDateFormat("_d-M-yyyy_h-mm-ss");
	    df.setTimeZone(TimeZone.getTimeZone("Canada/Eastern"));
	    folder=df.format(new Date());
	    basefold=System.getProperty("user.dir") + "\\screenshot\\"+folder+"\\";
	    
		ExtentHtmlReporter extentHtmlReporter = new ExtentHtmlReporter(
				System.getProperty("user.dir") + "/Reports/extent.html");
		extentReports = new ExtentReports();
		extentReports.attachReporter(extentHtmlReporter);
		
	}

	@BeforeMethod
	@Parameters({"device","platformVersion","url"})
	public void setUp(String device,String  platformVersion,String url) {
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("platformName", Constants.PLATFORM_NAME);
		capabilities.setCapability("platformVersion",platformVersion);
		capabilities.setCapability("deviceName", device);
		capabilities.setCapability("appPackage", Constants.APP_PACKAGE);
		capabilities.setCapability("appActivity", Constants.APP_ACTIVITY);
		try {
			driver = new AndroidDriver<AndroidElement>(new URL(url), capabilities);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		atest+=count+". ";
		count++;
	}

	@AfterMethod
	public void flushSetUp(ITestResult result) throws IOException {
		System.out.println();
		if (result.getStatus() == ITestResult.SUCCESS) {
			logger.pass("Test Passed",
					MediaEntityBuilder.createScreenCaptureFromPath(CommonFunctions.getScreenShot(driver)).build());
			CommonFunctions.getScreenShotAll(driver,basefold+result.getName()+"_pass");
			atest+=result.getTestClass().getName()+"."+result.getName()+" - Passed\n";
		} else if (result.getStatus() == ITestResult.FAILURE) {
			logger.fail("Test Failed",
					MediaEntityBuilder.createScreenCaptureFromPath(CommonFunctions.getScreenShot(driver)).build());
			CommonFunctions.getScreenShotAll(driver,basefold+result.getName()+"_fail");
			atest+=result.getTestClass().getName()+"."+result.getName()+" - Failed\n";
		} else if (result.getStatus() == ITestResult.SKIP) {
			logger.skip("Test skipped",
					MediaEntityBuilder.createScreenCaptureFromPath(CommonFunctions.getScreenShot(driver)).build());
			atest+=result.getTestClass().getName()+"."+result.getName()+" - Skipped\n";
		}
		extentReports.flush();
		
		driver.quit();

	}

	@AfterSuite
	public void sendStatusToSlack() throws IOException {
		
		SendMailForFailedScenarios.SendMail(folder,atest);

		slackMessage slackMsg = slackMessage.builder().username("user")
		.text("-----------------Test status------------------\n"
		+"| No | PackageName.className.TestCaseName | status |\n"+
				atest+
			"---------------------------------------------").icon_emoji(":twice:").build();
		slackUtils.sendMessage(slackMsg);

		
		
		
		
	}
	
}
