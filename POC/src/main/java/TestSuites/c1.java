package TestSuites;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.agiletestware.pangolin.annotations.Pangolin;
import com.aventstack.extentreports.MediaEntityBuilder;

import common.CommonFunctions;
import common.SetUp;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import pages.HomePage;
import pages.LoginPage;
import pages.TimeAttendance;
import resources.Constants;
/**
 * @author Bobby
 */
@Pangolin(sectionPath = "Master\\Section\\WebDriver1")
public class c1 extends SetUp {

	

	@Test
	public void c1() throws InterruptedException, IOException {
		
		logger = extentReports.createTest("C1_Employee can punch for clock in/out. start/end rest or start/end meal");
		//atest+="C1_Employee can punch for clock in/out. start/end rest or start/end meal";
		TimeZone timeZone = TimeZone.getTimeZone("Canada/Eastern");
		DateFormat dateFormat = new SimpleDateFormat("h:mm a");
		dateFormat.setTimeZone(TimeZone.getTimeZone("Canada/Eastern"));
		WebDriverWait wait;
		wait = new WebDriverWait(driver, 20);
		Properties property = CommonFunctions.getProperty(Constants.CREDENTIAL_FILE_PATH);
		LoginPage l = new LoginPage(driver);
		l.getLoginFunctionality(property.getProperty("Employee1UserName"), property.getProperty("Employee1Password"));
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		TimeAttendance t = new TimeAttendance(driver);
		HomePage homePage = new HomePage(driver);
		homePage.getButtonSideMenu().click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		CommonFunctions.selectElementUsingText(Constants.TIME_ATTENDANCE, driver).click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		t.getClockIn().click();
		t.getPunch().click();
		String clockintime=dateFormat.format(new Date());
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		Assert.assertEquals(true,t.getPopup().isDisplayed(),"popup not displayed");
		
		
		t.getBreakIn().click();
		t.getPunch().click();
		String breakintime=dateFormat.format(new Date());
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		Assert.assertEquals(true,t.getPopup().isDisplayed(),"popup not displayed");
		
		t.getBreakOut().click();
		t.getPunch().click();
		String breakouttime=dateFormat.format(new Date());
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		Assert.assertEquals(true,t.getPopup().isDisplayed(),"popup not displayed");
		
		t.getMealIn().click();
		t.getPunch().click();
		String mealintime=dateFormat.format(new Date());
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		Assert.assertEquals(true,t.getPopup().isDisplayed(),"popup not displayed");
		
		t.getMealOut().click();
		t.getPunch().click();
		String mealouttime=dateFormat.format(new Date());
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		Assert.assertEquals(true,t.getPopup().isDisplayed(),"popup not displayed");
		
		t.getClockOut().click();
		t.getPunch().click();
		String clockouttime=dateFormat.format(new Date());
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		Assert.assertEquals(true,t.getPopup().isDisplayed(),"popup not displayed");
	
		t.getTimecard().click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		List<MobileElement> weekDateCards = t.getCurrentDateRange();		
		String week = CommonFunctions.getCurrentWeek(driver);
		String date = CommonFunctions.getCurrentDate(driver);
		//System.out.println((weekDateCards.get(weekDateCards.size()-1).getText())+" :out: "+week);
		//System.out.println("week: "+weekDateCards.get(weekDateCards.size()-1).getText()+" week: "+week);
		Assert.assertEquals((weekDateCards.get(weekDateCards.size()-1).getText()).contains(week), true,"current week not found"); 
		
		weekDateCards.get(weekDateCards.size()-1).click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		CommonFunctions.scrollToElementUsingText(driver, CommonFunctions.getCurrentDatePlus2Date(driver));
		List<MobileElement> currentDateCards=t.getCurrentDate();
		List<MobileElement> timeLogs=t.getTimeLogs();
		//System.out.println("length:"+(currentDateCards.get(currentDateCards.size()-1).getText())+" date:"+date);
		Assert.assertEquals((currentDateCards.get(currentDateCards.size()-1).getText()).contains(date), true,"current day not found"); 		
   		
  		
    		Assert.assertEquals((timeLogs.get(timeLogs.size()-1).getText()), clockouttime,"Not found clockout time");
    		Assert.assertEquals((timeLogs.get(timeLogs.size()-2).getText()), mealouttime,"Not found mealout time");	
    		Assert.assertEquals((timeLogs.get(timeLogs.size()-3).getText()), mealintime,"Not found mealin time");
    		Assert.assertEquals((timeLogs.get(timeLogs.size()-4).getText()), breakouttime,"Not found breakout time");
    		Assert.assertEquals((timeLogs.get(timeLogs.size()-5).getText()), breakintime,"Not found breakin time");
    		Assert.assertEquals((timeLogs.get(timeLogs.size()-6).getText()), clockintime,"Not found clockin time");

    	
    		
    		
		

	}

}
