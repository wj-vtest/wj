package TestSuites;

import java.io.FileNotFoundException;
import java.util.Properties;
import org.testng.Assert;
import org.testng.annotations.Test;
import common.CommonFunctions;
import common.SetUp;
import pages.ChannelsPage;
import pages.HomePage;
import pages.LoginPage;
import resources.Constants;
import com.agiletestware.pangolin.annotations.Pangolin;
/**
 * @author Ranjit
 */

@Pangolin(sectionPath = "Master\\Section\\WebDriver")
public class ChannelsTestSuite extends SetUp {
	
	@Test(priority = 1)
	public void EmpCanPostC10() throws FileNotFoundException, InterruptedException {
		logger = extentReports.createTest("C10_Employee can post");
		
		Properties property = CommonFunctions.getProperty(Constants.CREDENTIAL_FILE_PATH);
		String randomString = CommonFunctions.generateRandomString(8);
		LoginPage loginPage = new LoginPage(driver);
		loginPage.getLoginFunctionality(property.getProperty("Employee1UserName"),
				property.getProperty("Employee1Password"));
		HomePage homePage = new HomePage(driver);
		homePage.getButtonSideMenu().click();
		CommonFunctions.selectElementUsingText(Constants.CHANNELS, driver).click();
		ChannelsPage channelsPage = new ChannelsPage(driver);
		String channelName = channelsPage.getFirstChannel().getText();
		channelsPage.getFirstChannel().click();
		Assert.assertEquals(channelName, channelsPage.getTextChannelHeader().getText());
		channelsPage.getButtonPencil().click();
		Assert.assertEquals(channelsPage.getTextCreateAPost().getText(), Constants.CREATE_POST);
		channelsPage.getTextAreaEditPost().sendKeys(randomString);
		channelsPage.getButtonSend().click();
		CommonFunctions.scrollToElementUsingText(driver, randomString);
		Assert.assertEquals(channelsPage.getTextPostName().get(0).getText(), randomString);

	}

	@Test(priority = 2)
	public void EmpCanDeleteC11() throws FileNotFoundException, InterruptedException {
		logger = extentReports.createTest("Employee Can delete a post");
		LoginPage loginPage = new LoginPage(driver);
		Properties property = CommonFunctions.getProperty(Constants.CREDENTIAL_FILE_PATH);
		String randomString = CommonFunctions.generateRandomString(8);
		loginPage.getLoginFunctionality(property.getProperty("Employee1UserName"),
				property.getProperty("Employee1Password"));
		HomePage homePage = new HomePage(driver);
		homePage.getButtonSideMenu().click();
		CommonFunctions.selectElementUsingText(Constants.CHANNELS, driver).click();
		ChannelsPage channelsPage = new ChannelsPage(driver);
		channelsPage.getFirstChannel().click();
		channelsPage.getButtonPencil().click();
		channelsPage.getTextAreaEditPost().sendKeys(randomString);
		channelsPage.getButtonSend().click();
		CommonFunctions.scrollToElementUsingText(driver, randomString);
		Assert.assertEquals(channelsPage.getTextPostName().get(0).getText(), randomString);
		channelsPage.getButtonPostEditMenus().get(0).click();
		Assert.assertEquals(CommonFunctions.selectElementUsingText(Constants.DELETE_POST, driver).getText(),
				Constants.DELETE_POST);
		Assert.assertEquals(CommonFunctions.selectElementUsingText(Constants.EDIT_POST, driver).getText(),
				Constants.EDIT_POST);
		System.out.println(CommonFunctions.selectElementUsingText(Constants.DELETE_POST, driver).isEnabled());
		CommonFunctions.selectElementUsingText(Constants.DELETE_POST, driver).click();
		Assert.assertNotEquals(channelsPage.getTextPostName().get(0).getText(), randomString);

	}

	@Test(priority = 3)
	public void EmpCanEditC12() throws FileNotFoundException, InterruptedException {
		logger = extentReports.createTest("Employee can edit");
		LoginPage loginPage = new LoginPage(driver);
		Properties property = CommonFunctions.getProperty(Constants.CREDENTIAL_FILE_PATH);
		String randomString = CommonFunctions.generateRandomString(8);
		String randomString2 = CommonFunctions.generateRandomString(8);
		loginPage.getLoginFunctionality(property.getProperty("Employee1UserName"),
				property.getProperty("Employee1Password"));
		HomePage homePage = new HomePage(driver);
		homePage.getButtonSideMenu().click();
		CommonFunctions.selectElementUsingText(Constants.CHANNELS, driver).click();
		ChannelsPage channelsPage = new ChannelsPage(driver);
		channelsPage.getFirstChannel().click();
		channelsPage.getButtonPencil().click();
		channelsPage.getTextAreaEditPost().sendKeys(randomString);
		channelsPage.getButtonSend().click();
		CommonFunctions.scrollToElementUsingText(driver, randomString);
		Assert.assertEquals(channelsPage.getTextPostName().get(0).getText(), randomString+"12");
		channelsPage.getButtonPostEditMenus().get(0).click();
		Assert.assertEquals(CommonFunctions.selectElementUsingText(Constants.DELETE_POST, driver).getText(),
				Constants.DELETE_POST);
		Assert.assertEquals(CommonFunctions.selectElementUsingText(Constants.EDIT_POST, driver).getText(),
				Constants.EDIT_POST);
		CommonFunctions.selectElementUsingText(Constants.EDIT_POST, driver).click();
		Assert.assertEquals(channelsPage.getHeaderEditPost().getText(), Constants.EDIT_POST);
		channelsPage.getTextAreaEditPost().sendKeys(randomString2);
		System.out.println(channelsPage.getTextAreaEditPost().isEnabled());
		channelsPage.getButtonSend().click();
		Thread.sleep(6000);
		driver.navigate().back();
		channelsPage.getFirstChannel().click();
		CommonFunctions.scrollToElementUsingText(driver, randomString2);
		Assert.assertEquals(channelsPage.getTextPostName().get(0).getText(), randomString2);

	}

}
