package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import common.SetUp;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class SurveyPage {

	WebDriver driver;

	public SurveyPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(name = "username")
	private WebElement username;

	public WebElement getContinueButton() {
		return continueButton;
	}

	public WebElement getPassword() {
		return password;
	}

	public WebElement getLogInButton() {
		return logInButton;
	}

	public WebElement getSurveyLink() {
		return surveyLink;
	}

	public WebElement getAddSurveyButton() {
		return addSurveyButton;
	}

	public WebElement getSurveyName() {
		return surveyName;
	}

	public WebElement getSurveyType() {
		return surveyType;
	}

	public WebElement getSurveyBadges() {
		return surveyBadges;
	}

	public WebElement getTargetAudience() {
		return targetAudience;
	}

	public WebElement getSurveyStatus() {
		return surveyStatus;
	}

	public WebElement getAddQuestion() {
		return addQuestion;
	}

	public WebElement getQuestionType() {
		return questionType;
	}

	public WebElement getQuestion() {
		return question;
	}

	public WebElement getSaveButton() {
		return saveButton;
	}

	public WebElement getAddAnswer() {
		return addAnswer;
	}

	public WebElement getCreateSurveyButton() {
		return createSurveyButton;
	}

	public WebElement getUsername() {
		return username;
	}

	@FindBy(xpath = "//button[text()='Continue']")
	private WebElement continueButton;

	@FindBy(name = "password")
	public WebElement password;

	@FindBy(xpath = "//button[text()='Log In']")
	private WebElement logInButton;

	@FindBy(xpath = "//span[text()='Surveys']")
	private WebElement surveyLink;

	@FindBy(xpath = "//button[text()='Add survey']")
	private WebElement addSurveyButton;

	@FindBy(id = "surveyName-")
	private WebElement surveyName;

	@FindBy(xpath = "//select[@id='type']")
	private WebElement surveyType;

	@FindBy(id = "surveyBadges")
	private WebElement surveyBadges;

	@FindBy(id = "segmentationList")
	private WebElement targetAudience;

	@FindBy(id = "surveyStatus")
	private WebElement surveyStatus;

	@FindBy(xpath = "//span[text()='Add question']")
	private WebElement addQuestion;

	@FindBy(id = "questionType")
	private WebElement questionType;

	@FindBy(xpath = "//input[@name='question-' and @required='required']")
	private WebElement question;

	@FindBy(xpath = "//input[@value='Save']")
	private WebElement saveButton;

	@FindBy(xpath = "//span[text()='Add answer']")
	private WebElement addAnswer;

	@FindBy(xpath = "//input[@value='Create survey']")
	private WebElement createSurveyButton;

}
