package pages;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

/**
 * @author Ranjit
 */
public class HomePage {

	MobileDriver<MobileElement> driver;

	public HomePage(MobileDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	@AndroidFindBy(accessibility = "Navigate up")
	private MobileElement buttonSideMenu;

	@AndroidFindBy(xpath = "//*[contains(@text,'vTest-POC')]")
	private MobileElement header;

	public MobileElement getButtonSideMenu() {
		return buttonSideMenu;
	}

	public MobileElement getHeader() {
		return header;
	}
}
