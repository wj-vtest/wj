package pages;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
/**
 * @author Ranjit
 */
public class LoginPage {

	MobileDriver<MobileElement> driver;

	public LoginPage(MobileDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	@AndroidFindBy(id = "login_username_auto_complete_text_view")
	private MobileElement textBoxUserName;

	@AndroidFindBy(id = "login_password_edit_text")
	private MobileElement textBoxPassword;

	@AndroidFindBy(id = "login_next_button")
	private MobileElement buttonContinue;

	@AndroidFindBy(id = "login_password_log_in_button")
	private MobileElement buttonLogin;

	public MobileElement getTextBoxUserName() {
		return textBoxUserName;
	}

	public MobileElement getTextBoxPassword() {
		return textBoxPassword;
	}

	public MobileElement getButtonContinue() {
		return buttonContinue;
	}

	public MobileElement getButtonLogin() {
		return buttonLogin;
	}

	public void getLoginFunctionality(String username, String password) throws InterruptedException {
		LoginPage loginPage = new LoginPage(driver);
		loginPage.getTextBoxUserName().sendKeys(username);
		loginPage.getButtonContinue().click();
		loginPage.getTextBoxPassword().sendKeys(password);
		loginPage.getButtonLogin().click();
		Thread.sleep(2000);
		HomePage homePage = new HomePage(driver);
		String[] expected = username.toUpperCase().split("@");
		String[] actual=homePage.getHeader().getText().split(" ");
		if (actual[1].contains(expected[0])) {
			System.out.println("Login is successful");
		}

	}

}
