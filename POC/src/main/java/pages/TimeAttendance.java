package pages;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidBy;
import io.appium.java_client.pagefactory.AndroidFindAll;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
/**
 * @author Bobby
 */
public class TimeAttendance {


	private MobileDriver driver;
		
		public TimeAttendance(MobileDriver driver)
		{
			this.driver=driver;
			PageFactory.initElements(new AppiumFieldDecorator(driver), this);
			
		}
		
		
		    @AndroidFindBy(xpath = "//android.widget.ImageButton[@content-desc=\"Navigate up\"]")
		    private MobileElement menu;
		    
		    @AndroidFindBy(xpath = "//hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout[1]/androidx.recyclerview.widget.RecyclerView/androidx.appcompat.widget.LinearLayoutCompat[4]/android.widget.CheckedTextView\n" + 
		    		"")
		    private MobileElement tna1;
		    
		    @AndroidFindBy(id = "com.workjam.workjam.qa:id/punch_clock_shift_start_checked_text_view")
		    private MobileElement clockin;
		    
		    @AndroidFindBy(id = "com.workjam.workjam.qa:id/punch_clock_shift_end_checked_text_view")
		    private MobileElement clockout;
		    
		    @AndroidFindBy(id = "com.workjam.workjam.qa:id/punch_clock_break_start_checked_text_view")
		    private MobileElement breakin;

		    @AndroidFindBy(id = "com.workjam.workjam.qa:id/punch_clock_break_end_checked_text_view")
		    private MobileElement breakout;
		   
		    @AndroidFindBy(id = "com.workjam.workjam.qa:id/punch_clock_meal_start_checked_text_view")
		    private MobileElement mealin;
		    
		    @AndroidFindBy(id = "com.workjam.workjam.qa:id/punch_clock_meal_end_checked_text_view")
		    private MobileElement mealout;
		    
		    
		    @AndroidFindBy(id = "com.workjam.workjam.qa:id/punch_clock_punch_button")
		    private MobileElement punch;
		    
		    @AndroidFindBy(xpath = "//hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout")
		    private MobileElement popup;
		    
		    @AndroidFindBy(xpath = "//androidx.appcompat.app.a.c[@content-desc=\"Timecards\"]")
		    private MobileElement timecards;

		    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.viewpager.widget.ViewPager/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.TextView[1]")
		    private List<MobileElement> currentDateRange;
		    
		    @AndroidFindAll(@AndroidBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.viewpager.widget.ViewPager/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.TextView[1]"))
		    private List<MobileElement> currentDate; 
		    
		    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.viewpager.widget.ViewPager/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[1]")
		    private List<MobileElement> timeRecords;
		    
		    
		    
		    public MobileElement Menu() {
		        return menu;
		    }
		    public MobileElement getTimenAttendance()
		    {
		    	return tna1;
		    }
		  
		    public MobileElement getClockIn()
		    {
		    	return clockin;
		    }
		    public MobileElement getClockOut()
		    {
		    	return clockout;
		    }
		    
		    public MobileElement getBreakIn()
		    {
		    	return breakin;
		    }
		    public MobileElement getBreakOut()
		    {
		    	return breakout;
		    }
		    
		    public MobileElement getMealIn()
		    {
		    	return mealin;
		    }
		    public MobileElement getMealOut()
		    {
		    	return mealout;
		    }
		    
		    public MobileElement getPunch()
		    {
		    	return punch;
		    }
		    public MobileElement getPopup()
		    {
		    	return popup;
		    	
		    }
		    
		    public MobileElement getTimecard()
		    {
		    	return timecards;
		    }
		   public  List<MobileElement> getCurrentDateRange()
		   {
			return currentDateRange;
			   
		   }
		   public  List<MobileElement> getCurrentDate()
		   {
			return currentDate;
			   
		   }
		   public  List<MobileElement> getTimeLogs()
		   {
			return timeRecords;
			   
		   }
		    }



