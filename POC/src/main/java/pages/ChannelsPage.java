package pages;

import java.util.ArrayList;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
/**
 * @author Ranjit
 */
public class ChannelsPage {

	MobileDriver<MobileElement> driver;

	public ChannelsPage(MobileDriver driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	@AndroidFindBy(id = "channel_floatingActionButton")
	private MobileElement buttonPencil;

	@AndroidFindBy(xpath = "//*[@text='Create a post']")
	private MobileElement textCreateAPost;

	@AndroidFindBy(id = "channelPostEdit_editText")
	private MobileElement textAreaEditPost;

	@AndroidFindBy(id = "channel_post_content_text_view")
	private ArrayList<MobileElement> textPostNames;

	@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.view.ViewGroup/android.widget.TextView")
	private MobileElement textChannelHeader;

	@AndroidFindBy(id = "channelItem_name_textView")
	private ArrayList<MobileElement> textChannelNames;

	@AndroidFindBy(accessibility = "Send")
	private MobileElement buttonSend;

	@AndroidFindBy(id = "channel_post_edit_menu")
	private ArrayList<MobileElement> buttonPostEditMenus;
	
	@AndroidFindBy(xpath = "//*[@text='Edit post']")
	private MobileElement headerEditPost;

	public MobileElement getHeaderEditPost() {
		return headerEditPost;
	}

	public MobileElement getButtonPencil() {
		return buttonPencil;
	}

	public MobileElement getTextCreateAPost() {
		return textCreateAPost;
	}

	public MobileElement getTextAreaEditPost() {
		return textAreaEditPost;
	}

	public ArrayList<MobileElement> getTextPostName() {
		return textPostNames;
	}

	public MobileElement getTextChannelHeader() {
		return textChannelHeader;
	}

	public ArrayList<MobileElement> getTextChannelNames() {
		return textChannelNames;
	}

	public MobileElement getButtonSend() {
		return buttonSend;

	}

	public ArrayList<MobileElement> getButtonPostEditMenus() {
		return buttonPostEditMenus;
	}

	public MobileElement getFirstChannel() {
		MobileElement element = getTextChannelNames().get(0);
		return element;
	}
	

}
